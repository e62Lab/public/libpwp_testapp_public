This repository contains libPwp test application for Android 4.4+.
This application will connect to PCARD-compatible devices (e.g. Savvy) and strat streaming ECG data to a local file (sdcard/ecg.txt)

Dependencies:
 - libPwp
 - libPcard
 - libPcardTimeAlign
 - libAndroidBleWrapper
 